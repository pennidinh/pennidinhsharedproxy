#!/usr/bin/env bash

# install to crontab with: @hourly bash -c "cd /home/ec2-user/pennidinhsharedproxy && ./updateAndRestart.sh"

cd /pennidinhsharedproxy

git pull --rebase

/usr/local/bin/docker-compose pull

/usr/local/bin/docker-compose up -d --remove-orphans
